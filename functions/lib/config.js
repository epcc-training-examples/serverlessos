// Abstracts environment variables

module.exports.POSTMARK_API_TOKEN = process.env.POSTMARK_API_TOKEN;
module.exports.POSTMARK_FROM_ADDRESS = process.env.POSTMARK_FROM_ADDRESS;
module.exports.POSTMARK_CONFIRMATION_TEMPLATE_ID =
  process.env.POSTMARK_CONFIRMATION_TEMPLATE_ID;
module.exports.MT_WEBHOOK_SECRET = process.env.MT_WEBHOOK_SECRET;
